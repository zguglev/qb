package com.biranful.qb.v2.utils;

import java.util.Collection;
import java.util.Iterator;

public class QueryBuilder {

    private QueryInfo queryInfo;

    QueryBuilder() {
        queryInfo = new QueryInfo();
    }

    public static QueryBuilder builder() {
        return new QueryBuilder();
    }

    public Select select() {
        return new Select(queryInfo);
    }

    public Insert insert() {
        return new Insert(queryInfo);
    }

    static String processColumns(Collection<String> columns) {
        StringBuilder sb = new StringBuilder();

        Iterator<String> iterator = columns.iterator();
        if (iterator.hasNext()) {
            String first = iterator.next();
            if (first != null) {
                sb.append(first);
            }
        }

        while (iterator.hasNext()) {
            sb.append(',');

            String obj = iterator.next();
            if (obj != null) {
                sb.append(obj);
            }
        }

        return sb.toString();
    }

}
