package com.biranful.qb.v1;

import com.biranful.qb.ReflectionUtil;
import com.biranful.qb.entity.Base;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QueryBuilderImpl<T> implements QueryBuilder {

    private Class<T> beanType;

    public QueryBuilderImpl(Class<T> tClass) {
        beanType = tClass;
    }

    @Override
    public QueryInfo select() {
        return select(null);
    }

    public QueryInfo select(String[] where) {
        String table = beanType.getSimpleName().toLowerCase();

        StringBuilder stringBuilder = new StringBuilder("SELECT * FROM ");
        stringBuilder.append(table);

        if (where != null && where.length > 0)
            return applyWhere(stringBuilder, where);

        return new QueryInfo(stringBuilder.toString(), new HashMap<>());
    }

    @Override
    public QueryInfo insert() {
        List<Field> fields = ReflectionUtil.getClassFields(beanType, new ArrayList<>());
        StringBuilder queryBuilder = new StringBuilder("INSERT INTO ");
        queryBuilder.append(beanType.getSimpleName().toLowerCase())
                .append(" (");

        Map<Integer, String> placeholders = new HashMap<>();

        StringBuilder names = new StringBuilder();
        StringBuilder questionMarks = new StringBuilder();
        for (int i = 0; i < fields.size() - 1; i++) {
            Field field = fields.get(i);

            String name = field.getName();
            if (Base.class.isAssignableFrom(field.getType())) {
                name += "_id";
            }

            names.append(name)
                    .append(", ");
            questionMarks.append("?")
                    .append(", ");

            placeholders.put(i, name);
        }

        queryBuilder.append(names.replace(names.lastIndexOf(","), names.length(), ""))
                .append(") VALUES (")
                .append(questionMarks.replace(questionMarks.lastIndexOf(","), names.length(), ""))
                .append(')');

        return new QueryInfo(queryBuilder.toString(), placeholders);
    }

    private QueryInfo applyWhere(StringBuilder queryBuilder, String[] where) {
        Map<Integer, String> placeholders = new HashMap<>();

        queryBuilder.append(" WHERE ");
        for (int i = 0; i < where.length; i++) {
            queryBuilder.append(where[i])
                    .append(" = ?");
            placeholders.put(i, where[i]);
        }

        return new QueryInfo(queryBuilder.toString(), placeholders);
    }
}
